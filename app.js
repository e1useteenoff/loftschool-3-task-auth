const createError = require('http-errors')
const express = require('express')
const path = require('path')
const logger = require('morgan')
const flash = require('connect-flash')
const session = require('express-session')
require('dotenv').config()

const mainRouter = require('./routes/')

const app = express()

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

process.env.NODE_ENV === 'development'
  ? app.use(logger('dev'))
  : app.use(logger('short'))

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use(express.static(path.join(__dirname, 'public')))
app.use(flash())

app.use(
  session({
    saveUninitialized: true,
    secret: process.env.SESSION_SECRET_KEY,
    key: 'sessionkey',
    cookie: { path: '/', httpOnly: true, maxAge: 86400000 },
    resave: false,
  })
)

app.use('/', mainRouter)

app.use((req, __, next) => {
  next(
    createError(404, `Ой, извините, но по пути ${req.url} ничего не найдено!`)
  )
})

app.use((err, req, res, next) => {
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  res.status(err.status || 500)
  res.render('error')
})

const port = process.env.PORT || 3000
app.listen(port, () => {
  console.log('Running on ', port)
})
